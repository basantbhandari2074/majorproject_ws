# write the performance evalution code based on the feature matching
# how the generated midi file feature are similar to the real dataset

# If validation during training, you can monitor the loss.
# If you want to validate whether the generated music is close to real music,
# simplest is to listen to the generated music.
# However that needs some manual work.
# If you want to automate it, maybe you can look into some feature checking
# in MIDI e.g. checking if the notes stay in the same key,
# if the speed is not too fast/slow, etc.

import os, midi, math, random
import numpy as np


GENERATED_MUSIC_FILEPATH="code/outcomes/sample1.mid"
REALDATA_MUSIC_FILEPATH="code/data/hip_hop/50_cent/50_cent-candy_shop.mid"





# TODO 3: Plot the loss graph

# TODO 4: check the features



# TODO 5: listen the generated music
