from django.shortcuts import render

import os
from argparse import ArgumentParser
import numpy as np
import torch

from mainapp.c_rnn_gan import Generator
from mainapp import music_data_utils
import pygame
import time


CKPT_DIR = 'mainapp/models'
G_FN = 'c_rnn_gan_g.pth'

MAX_SEQ_LEN = 256
Delay_Value = 6

pygame.init()


# load and prepare model
dataloader = music_data_utils.MusicDataLoader(datadir=None)
num_feats = dataloader.get_num_song_features()

use_gpu = torch.cuda.is_available()
g_model = Generator(num_feats, use_cuda=use_gpu)

if not use_gpu:
    ckpt = torch.load(os.path.join(CKPT_DIR, G_FN), map_location='cpu')
else:
    ckpt = torch.load(os.path.join(CKPT_DIR, G_FN))

g_model.load_state_dict(ckpt)



# Create your views here.
def HomePage(request):
    if request.POST:
        music_duration = request.POST.get('music_duration',0)
        music_duration = int(music_duration)
        if request.POST.get('play', False):
            # play music and update flag
            g_states = g_model.init_hidden(1)
            z = torch.empty([1, MAX_SEQ_LEN, num_feats]).uniform_() # random vector
            if use_gpu:
                z = z.cuda()
                g_model.cuda()
            g_model.eval()


            for i in range(0, music_duration):
                g_feats, g_states = g_model(z, g_states)
                song_data = g_feats.squeeze().cpu()
                song_data = song_data.detach().numpy() 
                dataloader.save_data("media/sample.midi", song_data)
                # play each tone using pygame
                pygame.mixer.music.load("media/sample.midi")
                pygame.mixer.music.play()
                time.sleep(Delay_Value)



        if request.POST.get('stop', False):
            # stop music
            if pygame.mixer.get_busy():
                pygame.mixer.music.stop()


        if request.POST.get('pause', False):
            # pause music
            if pygame.mixer.get_busy():
                pygame.mixer.pause()


        if request.POST.get('resume', False):
            # resume music
            if not pygame.mixer.get_busy():
                pygame.mixer.unpause()

        return render(request, 'mainapp/home.html',{})


    else:
        return render(request, 'mainapp/home.html',{})







def AboutPage(request):
    return render(request, 'mainapp/about.html', {})



def ContactPage(request):
    return render(request, 'mainapp/contact.html', {})


def RemixMusicPage(request):
    return render(request, 'mainapp/remixmusic.html', {})


