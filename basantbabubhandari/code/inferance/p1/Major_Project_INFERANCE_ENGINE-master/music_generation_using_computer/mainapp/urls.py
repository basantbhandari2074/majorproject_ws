from django.urls import path
from mainapp import views

urlpatterns = [
    path('', views.HomePage, name='home'),
    path('about/', views.AboutPage, name='about'),
    path('contact/', views.ContactPage, name='contact'),
    path('remixmusic/', views.RemixMusicPage, name='remixmusic'),
]


